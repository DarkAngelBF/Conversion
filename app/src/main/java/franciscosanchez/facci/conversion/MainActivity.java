package franciscosanchez.facci.conversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView Centigrados, Fahrenheit;
    EditText DigitosC, DigitosF;
    Button ConvertirC, ConvertirF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Centigrados = (TextView)findViewById(R.id.LblCenti);
        Fahrenheit = (TextView)findViewById(R.id.LblFaren);
        DigitosC = (EditText)findViewById(R.id.editTextCenti);
        DigitosF = (EditText)findViewById(R.id.editTextFahren);
        ConvertirC = (Button)findViewById(R.id.buttonFahren);
        ConvertirF = (Button)findViewById(R.id.buttonCenti);

        ConvertirC.setOnClickListener(this);
        ConvertirF.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFahren:
                if (DigitosF.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.validar), Toast.LENGTH_LONG).show();
                }else {
                    Double Fahren = Double.valueOf(DigitosF.getText().toString());
                    Double Centi = (Fahren - 32) / 1.8;
                    Toast.makeText(MainActivity.this, String.valueOf(Centi)+ " ºC", Toast.LENGTH_LONG).show();
                    Centigrados.setText(String.valueOf(Centi)+ " ºC");
                    DigitosF.setText("");
                }
                break;

            case R.id.buttonCenti:
                if (DigitosC.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.validar), Toast.LENGTH_LONG).show();
                }else{
                    Double Centig = Double.valueOf(DigitosC.getText().toString());
                    Double Fahrenh = (Centig *1.8 ) + 32;
                    Toast.makeText(MainActivity.this, String.valueOf(Fahrenh)+ " ºF", Toast.LENGTH_LONG).show();
                    Fahrenheit.setText(String.valueOf(Fahrenh)+ " ºF");
                    DigitosC.setText("");
                }
                break;

        }
    }
}
